<?php
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * @file
 * Snowflakes module.
 */

/**
 * Check whether snowflakes are enabled.
 *
 * @return bool
 *   TRUE, if snowflakes are enabled on the page.
 */
function snowflakes_is_enabled(): bool {
  return \Drupal::config('snowflakes.settings')->get('enabled');
}

/**
 * Check whether snowflakes should be displayed on the current page.
 *
 * @return bool
 *   TRUE, if snowflakes are enabled and should be displayed on the page.
 */
function snowflakes_should_display(): bool {
  return snowflakes_is_enabled() &&
    (!\Drupal::config('snowflakes.settings')->get('exclude_admin') ||
      !Drupal::service('router.admin_context')->isAdminRoute());
}

/**
 * Implements hook_theme().
 */
function snowflakes_theme($existing, $type, $theme, $path) {
  return [
    'snowflakes' => [
      'variables' => [],
    ],
    'snowflakes_onoff' => [
      'variables' => [],
    ],
  ];
}

/**
 * Implements hook_page_bottom().
 */
function snowflakes_page_bottom(array &$page_bottom) {
  if (!snowflakes_should_display()) {
    // Early exit, if snowflakes should not be displayed.
    return;
  }
  $page_bottom['snowflakes'] = [
    '#theme' => 'snowflakes',
  ];
  if (\Drupal::config('snowflakes.settings')->get('toggle_button')) {
    $page_bottom['snowflakes_onoff'] = [
      '#theme' => 'snowflakes_onoff',
    ];
  }
}

/**
 * Implements hook_page_attachments().
 */
function snowflakes_page_attachments(array &$attachments) {
  if (!snowflakes_should_display()) {
    // Early exit, if snowflakes should not be displayed.
    return;
  }
  $attachments['#attached']['library'][] = 'snowflakes/snowflakes';
  if (\Drupal::config('snowflakes.settings')->get('toggle_button')) {
    $attachments['#attached']['library'][] = 'snowflakes/onoff';
  }
}

/**
 * @file
 * Module's hooks implementations and helper functions.
 */

/**
 * Implements hook_help().
 */
function snowflakes_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.snowflakes':
      $readme = __DIR__ . '/README.md';
      $text = file_get_contents($readme);
      $output = '';

      // If the Markdown module is installed, use it to render the README.
      if ($text && \Drupal::moduleHandler()->moduleExists('markdown') === TRUE) {
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        $output = $filter->process($text, 'en');
      }
      // Else the Markdown module is not installed output the README as text.
      else if ($text) {
        $output = '<pre>' . $text . '</pre>';
      }

      // Add a link to the Drupal.org project.
      $output .= '<p>';
      $output .= t('Visit the <a href=":project_link">Snowflakes project page</a> on Drupal.org for more information.',[
        ':project_link' => 'https://www.drupal.org/project/snowflakes'
        ]);
      $output .= '</p>';

      return $output;
  }

}
