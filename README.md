Snowflakes
==========

This module brings Christmas and winter flair to your Drupal site by adding pure CSS animated snowflakes.

The module wraps the simple yet amazing [CSSnowflakes](https://github.com/pajasevi/CSSnowflakes) code to your Drupal
site without the need of touching any template files.

[Issue Tracker](https://www.drupal.org/project/issues/snowflakes?version=8.x)

## Requirements

* Drupal 9 or 10

## Installation

Install it like any other Drupal module.

See the [Drupal](https://www.drupal.org/docs/8/extending-drupal-8/installing-modules-composer-dependencies)
documentation for more details.

## Credits

Snowflakes module was originally developed and is currently maintained
by [Mag. Andreas Mayr](https://www.drupal.org/u/agoradesign).

The HTML and CSS code that is used is based on the work of [Pavel Ševčík](https://github.com/pajasevi)

All initial development was sponsored by
* [agoraDesign KG](https://www.agoradesign.at)
